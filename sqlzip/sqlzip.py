#! /usr/bin/env python3

import argparse as ap
from   datetime import datetime, date
import doctest
import hashlib
import importlib
import io
import math
from   pathlib import Path
import pkg_resources # part of setuptools
import pdb
import sqlite3 as sql3
import re
import string
import sys
import typing as tp
from   zlib import compress, decompress

sql_20211221_001_up = """
CREATE TABLE __archive_md ( name  TEXT
                          , type  TEXT
                          , value TEXT
                          );
CREATE TABLE documents  ( docid      INTEGER PRIMARY KEY AUTOINCREMENT
                        , name       TEXT
                        , sha256     TEXT
                        , compressor TEXT
                        , content    BLOB
                        );
CREATE INDEX documents_name ON documents(name);
CREATE TABLE metadata   ( docid INTEGER
                        , name  TEXT
                        , value TEXT
                        );
CREATE INDEX metadata_docid ON metadata(docid);
CREATE INDEX metadata_name  ON metadata(name);
CREATE INDEX metadata_value ON metadata(value);
CREATE TABLE __migrations ( id     TEXT
                          , source TEXT
                          );
INSERT INTO __archive_md(name,type,value) 
     VALUES ('compressor','str','zlib')
          , ('compression_level','int','-1')
          , ('version','str','20211221-001')
;
"""

# create a sqlarchive table if one does not exist
sql_20220105_001_up = """
CREATE TABLE sqlar ( docid INTEGER PRIMARY KEY  -- alias for rowid
                   , name  TEXT NOT NULL    -- document name
                   , mode  INT              -- mode
                   , mtime INT              -- modification time
                   , sz    INT              -- document size, 0 for directories or  metadata only
                   , data  BLOB             -- document content
                   , sha256 TEXT            -- SHA3-256 hash of content
                   , compressor TEXT        -- compressor library used to compress/decompress, default = 'zlib'
                   );
CREATE INDEX sqlar_name_idx ON sqlar(name);
PRAGMA application_id = 0x53515a50; -- 'SQZP'
"""

# This adds additional tables to a sqlarchive database
# to provide metadata for files in the archive.
sql_20220105_002_up = """
CREATE TABLE IF NOT EXISTS tmp_sqlar AS
    SELECT rowid  AS docid
         , name   AS name
         , mode   AS mode
         , mtime  AS mtime
         , sz     AS sz
         , data   AS data
         , NULL   AS sha256
         , 'zlib' AS compressor
    FROM sqlar
    ORDER BY rowid ASC;
DROP TABLE IF EXISTS sqlar;
ALTER TABLE tmp_sqlar RENAME TO sqlar;
CREATE UNIQUE INDEX sqlar_pri_idx ON sqlar(docid);
CREATE INDEX sqlar_name_idx ON sqlar(name);
PRAGMA application_id = 0x53515a50;
VACUUM;
"""

sql_20220105_003_up = """
CREATE TABLE __archive_md ( name  TEXT
                          , type  TEXT
                          , value TEXT
                          );
CREATE TABLE metadata   ( name  TEXT
                        , value TEXT
                        , docid INT  REFERENCES sqlar(docid)
                        );
CREATE UNIQUE INDEX metadata_idx  ON metadata(name,value,docid);
CREATE INDEX metadata_docid_idx ON metadata(docid,name);
INSERT INTO __archive_md(name,type,value) 
     VALUES ('compressor','str','zlib')
          , ('compression_level','int','-1')
          , ('version','str','20220105_002')
;
CREATE VIEW documents AS
    SELECT A.rowid      as docid,
           A.name       as name,
           A.mtime      as mtime,
           A.mode       as mode,
           A.sz         as size,
           A.sha256     as sha256,
           A.compressor as compressor,
           B.name       as md_name,
           B.value      as md_value
      FROM sqlar A
 LEFT JOIN metadata B
        ON B.docid = A.rowid
;
"""

pred_re   = re.compile(r'^(?P<name>[a-zA-Z0-9_:]+)\s*(?P<pred>[<>=!]=?|like)\s*(?P<value>.+)$',re.IGNORECASE)
conj_re = re.compile(r'^(?P<conj>@AND@|@ANDALL@|@OR@|@ORALL@)$',re.IGNORECASE)
basic_meta_names = ['docid','name','mode','mtime','size','sha256','compressor']

class SQLZipError(Exception):
    pass

def sha256sum(data) -> str:
    return hashlib.sha256(data).hexdigest()

def zlib_compress(data) -> bytearray:
    return compress(data)

def zlib_decompress(data) -> bytearray:
    return decompress(data)

class Query:
    """A list of Filters is compiled to a Query, which can be executed
    resulting in a list of document identifiers (docid).
    """

    def __init__(self, select:str, parms, basic_fields=None, only_md_fields=None) -> None:
        self.select       = select
        self.parms        = parms
        self.only_md_fields = only_md_fields
        self.basic_fields = basic_fields

    def __repr__(self) -> str:
        s = r'Query(select="{}",parms={})'.format(self.select,self.parms)
        return s

    def execute(self, cur: sql3.Cursor) -> sql3.Cursor:
        cur.execute(self.select,self.parms)
        return cur

class Filter:
    """Filters are used to select documents based on matches with
    metadata.
    """

    AND    = '@AND@'
    ANDALL = '@ANDALL@'
    OR     = '@OR@'
    ORALL  = '@ORALL@'

    def __init__(self, filter_str) -> None:
        """Parse a string into a Filter. 

        A typical filter looks like 'author == Mark Twain', with the
        seven comparison operators, < <= == = != >= >, supported.  The
        '=' operator is a shorthand for '=='. The SQL operator 'like'
        is also supported: 'title like %Huckleberry%' where the search
        value is passed through to the SQL query.
        
        We have four special operator values,
        @AND@, @ANDALL@, @OR@, and @ORALL@, which indicate we are to
        AND the two, or all, or OR two or all, filters on the stack
        when we compile the filter list into the WHERE clause.
        """

        self.name      = None
        self.predicate = None
        self.value     = None

        fm = pred_re.match(filter_str)
        if not fm:
            fm = conj_re.match(filter_str)
        if fm:
            try:
                conj = fm.group('conj')
                if conj == Filter.AND    or \
                   conj == Filter.ANDALL or \
                   conj == Filter.OR     or \
                   conj == Filter.ORALL  :
                    self.name = conj
            except IndexError:
                self.name = fm.group('name')
                try:
                    self.predicate = fm.group('pred')
                except IndexError:
                    raise IndexError('No valid match predicate found in {}'.format(filter_str))
                try:
                    v  = fm.group('value')
                    try:
                        x = float(v)
                        if x-math.floor(x) != 0.0:
                            x = int(v)
                        else:
                            x = v
                    except ValueError:
                        if self.name == 'mtime':
                            x = datetime.fromisoformat(v).timestamp()
                        else:
                            x = v
                    self.value = x
                except IndexError:
                    raise IndexError('No valid valid value found in {}'.format(filter_str))

    def __repr__(self) -> str:

        s = 'Filter(name="{}",predicate="{}",value="{}")'.format(self.name, self.predicate, self.value)
        return s
        

    def compile(fields, 
                filters,
                only_md_fields:str = None ) -> str:
        """
        Filter class function to compile a list of filters into a 
        SQL WHERE clause content.
        """

        stack: List[str] = []
        parms = []
        basic_fields = None

        if only_md_fields:
            basic_fields = []
            only_fields = only_md_fields.split(',')
            only_flds = []
            for i in range(0,len(only_fields)):
                f = only_fields[i]
                if f in basic_meta_names and f not in fields:
                    basic_fields.append(f)
                else:
                    only_flds.append(f)
            fields = fields + basic_fields

        for f in filters:
            if f is not None and f.name is not None:
                if f.name == Filter.AND:
                    if len(stack) >= 2:
                        f1 = stack.pop()
                        f0 = stack.pop()
                        stack.append("{} INTERSECT {}".format(f0,f1))
                elif f.name == Filter.ANDALL:
                    if len(stack) >= 2:
                        s = ' INTERSECT '.join(map( lambda flt : "{}".format(flt), stack ))
                        stack = [s]
                elif f.name == Filter.OR:
                    if len(stack) >= 2:
                        f1 = stack.pop()
                        f0 = stack.pop()
                        stack.append("{} UNION {}".format(f0,f1))
                elif f.name == Filter.ORALL:
                    if len(stack) >= 2:
                        s = ' UNION '.join(map( lambda flt : "{}".format(flt), stack ))
                        stack = [s]
                elif f.name.casefold() in basic_meta_names:
                    v = f.value
                    n = f.name.casefold()
                    stack.append("SELECT docid FROM documents WHERE {} {} ?".format(n,f.predicate))
                    parms.append(v)
                else:
                    v = f.value
                    stack.append("SELECT docid FROM documents WHERE md_name=? AND md_value {} ?".format(f.predicate))
                    parms.append(f.name)
                    parms.append(v)
        select = 'SELECT DISTINCT {} FROM documents WHERE docid IN (SELECT DISTINCT docid FROM ({}))'.format(",".join(fields), stack[0])

        if only_md_fields and only_flds and len(only_flds) > 0:
            only_clause = " AND ("+" OR ".join(["md_name == ?" for x in only_flds]) + ")"
            select = select + only_clause
            parms = parms + only_flds
        query =  Query(select,parms,basic_fields = basic_fields)
        return query


class Metadata:
    """Metadata represents values about an item in a SQLZip repository,
    or, perhaps, an item representedy ba a URI.

    Common metadata systems include Dublin Core (https://dublincore.org),
    schema.org (https://schema.org).
    """


    # offsets into returned rows
    NAME  = 0
    VALUE = 1
    DOCID = 2

    def __init__(self, name: str = "",
                       value: str = "" ) -> None:
        if not name or name.__class__.__name__ != 'str' :
            raise TypeError("Metadata name must be of type 'str'")
        self.name = name
        self.value = value

    def __eq__(self, other) -> bool:
        """__eq__(self,other) implements the standard equality check between two Metadata objects"""
        
        result = False
        if self.name == other.name and self.value == other.value:
            result = True
        return result

    def __lt__(self, other) -> bool:
        """__lt__(self,other) implements the standard less than comparison
        between two Metadata objects
        
        One issue to note is a value=None for a metadata object always compares
        larger than others having the same name.  The rationale for this decision
        lies with sorting situations.  Rather than having to iterate through a
        list of Metadata objects having a None value, these will sort to the end
        and thus one can stop processing the list when the first is encountered.
        """

        result = False
        if self.name < other.name:
            result = True
        elif self.name == self.name:
            if self.value and not other.value:
                result = True
            elif self.value and self.value < other.value:
                result = True
        return result

    def __repr__(self) -> str:
        if self.value.__class__.__name__ == 'str':
            return 'Metadata(name="{:s}",value="{}")'.format(self.name,self.value)
        else:
            return 'Metadata(name="{:s}",value={})'.format(self.name,self.value)

    def list_sortkey(md) -> str:
        return md.name+'\t'+md.value

class MetadataList:

    # metadata differences
    MDL_SAME          = 0
    MDL_DIFFER        = 1
    MDL_MISSING_SELF  = 2
    MDL_MISSING_OTHER = 3

    def diff(list_a:tp.List[Metadata],list_b:tp.List[Metadata]):
        list_a.sort(key=Metadata.list_sortkey)
        list_b.sort(key=Metadata.list_sortkey)

        self_i = 0
        other_i = 0

        while self_i < len(list_a) or other_i < len(list_b):
            if list_a[self_i] == list_b[other_i]:
                self_i  = self_i  + 1
                other_i = other_i + 1
            elif list_a[self_i].name < list_b[other_i].name:
                yield (MetadataList.MDL_MISSING_SELF,list_a[self_i].name,list_a[self_i].value)
                self_i  = self_i + 1
            elif list_a[self_i].name  == list_b[other_i].name and \
                 list_a[self_i].value != list_b[other_i].value:
                yield (MetadataList.MDL_DIFFER,list_b[other_i].name,list_a[self_i].value,list_b[other_i].value)
                self_i  = self_i  + 1
                other_i = other_i + 1
            elif list_b[other_i].name < list_a[self_i].name:
                yield (MetadataList.MDL_MISSING_OTHER,list_b[other_i].name,list_b[other_i].value)
                other_i  = other_i + 1
            else:
                raise Exception("Should not be here")



class Document:

    # offsets into returned rows
    DOCID    = 0
    NAME     = 1
    MODE     = 2
    MTIME    = 3
    SZ       = 4
    SIZE     = 4
    SHA256   = 5
    DATA     = 6
    METADATA = 7
    

    read_query = """SELECT docid
                         , name
                         , mode
                         , mtime
                         , sz
                         , sha256
                         , data
                         , compressor
                         FROM sqlar """

    def __init__(self, docid: int = 0,
                       name: str = "",
                       mode: int = 32768+6*64+4*8+4, # -rw-r--r--
                       mtime: int = 0,
                       data: bytearray = b"",
                       compressor: str = "zlib",
                       metadata: tp.List[Metadata] = [],
                       library = None
                ) -> None:
        """Create a new Document

        >>> lib = SQLZip()
        >>> doc = Document(name="Gettysburg Address", \
        ...                data= b'Four score and seven years ago,...', \
        ...                metadata=[Metadata(name='author',value='Lincoln, Abraham'), \
        ...                          Metadata(name='date',  value='1863-11-19')])
        >>> docid = lib.create_document(doc)
        """
        self.compressor = compressor
        self.data       = data
        self.docid      = docid
        self.library    = library
        self.metadata   = metadata
        self.mode       = mode
        self.mtime      = mtime
        self.name       = name


        if data is not None and len(data) > 0:
            self.data = data
            self.sz   = len(data)
            self.sha256 = sha256sum(data)
        else:
            self.data   = b""
            self.sha256 = ""
            self.sz     = 0
            

    def __repr__(self) -> str:
        sorted_meta = self.metadata.sort(key=Metadata.list_sortkey)
        if self.sz > 0 and self.data:
            content =  self.data.decode(encoding='utf-8')
        else:
            content = ""
        if self.library is None:
            libname = ""
        else:
            libname = self.library.name
        s = 'Document(docid={:d},name="{:s}",mode={:d},mtime={},sz={:d},sha256="{:s}",metadata={},library="{:s}")'.format(
                self.docid,
                self.name,
                self.mode,
                self.mtime,
                self.sz,
                self.sha256,
                self.metadata,
                libname)

        return s

    def __eq__(self, other) -> bool:
        """__eq__(self,other) implements the standard equality check between two documents, excluding
        the automatically assigned document identifier, docid."""

        result = True
        if not other:
            result = False
        elif self.name == other.name and self.sha256 == other.sha256:
            self.metadata.sort(key=Metadata.list_sortkey)
            other.metadata.sort(key=Metadata.list_sortkey)
            for i in range(0,len(self.metadata)):
                if i > len(other.metadata):
                    result = False
                    break
                elif self.metadata[i] != other.metadata[i]:
                    result = False
                    break
        elif self.mode    != other.mode or \
             self.mtime   != other.mtime or \
             self.sha256  != other.sha256 or \
             self.sz      != other.sz:
            result = False
        elif self.data != other.data:
            result = False
        
        return result


    def __lt__(self, other) -> bool:
        """__eq__(self,other) implements the standard equality check between two documents, excluding
        the automatically assigned document identifier, docid."""

        result = False
        if self.name == other.name and self.data == other.data:
            self_md  = self.metadata.sort(key=Metadata.list_sortkey)
            other_md = other.metadata.sort(key=Metadata.list_sortkey)
            for i in range(0,len(self_md)):
                if self_md[i].name < other_md[i].name:
                    result = True
                    break
                elif self_md[i].value < other_md[i].value:
                    result = True
                    break
        elif self.name < other.name:
            result = True
        
        return result

    def add_metadata(self, metadata:tp.List[Metadata]):
        """Add metadata items to Document"""

        if self.library is not None and self.docid > 0:
            metadata.sort(key=Metadata.list_sortkey)
            my_md: tp.List[Metadata] = []
            if self.metadata is not None:
                self_md  = self.metadata.sort(key=Metadata.list_sortkey)
            doc_md = []
            md = []
            i = 0
            for m in metadata:
                while i < len(my_md) and my_md[i] < m:
                    doc_md.append(my_md[i])
                    i = i + 1
                if i < len(my_md) and my_md[i] != m:
                    doc_md.append(m)
                md.append({"name":m.name,"value":m.value,"docid":self.docid})

            cur = self.library.connection.cursor()
            cur.execute("BEGIN TRANSACTION")
            cur.executemany("INSERT OR REPLACE INTO metadata (name,value,docid) VALUES (:name,:value,:docid)", md)
            cur.connection.commit()
            self.metadata = doc_md
        
        return self

    
    def delete_metadata(self, metadata:tp.List[Metadata]):
        """Delete the metadata items from existing metadata items for Document"""

        if self.library is not None and self.docid > 0:
            cur = self.library.connection.cursor()
            cur.execute("BEGIN TRANSACTION")
            for m in metadata:
                cur.execute("""DELETE
                                 FROM metadata
                                WHERE docid = :docid
                                  AND name  = :name
                                  AND value = :value""", {"docid":self.docid,
                                                          "name":m.name,
                                                          "value":m.value})
            self.library.connection.commit()
        
        return 
    
    def content(self) -> bytearray:
        """Return the content of a document or None if there is a
        problem accessing the data. It can consume considerable
        resources to retrieve a large document from the library, so
        we separate the document information from its content for
        efficiency reasons."""

        cnt = None
        if self.library is None or self.docid <= 0:
            cnt = None
        elif self.data is not None:
            cnt = self.data
        else:
            try:
                cur = self.library.connection.cursor()
                res = cur.execute("SELECT compressor,sz,sha256,data FROM sqlar WHERE docid = ?",(self.docid,))
                row = res.fetchone()
                compressor = row['compressor']
                rawdata    = row['data']
                sha256     = row['sha256']
                sz         = row['sz']
            except Exception:
                return None

            if self.library.compress_lib and rawdata:
                cnt = self.library.compress_lib.decompress(rawdata)
            else:
                cnt = rawdata
            self.data = cnt

        return cnt


    def diff(self, other):
        """Compute the difference between two documents, comparing all
        fields and metadata, returning a two element list of comparison
        results.

        """

        doc_diff  = [1,1,1,1,1,1,1,1]

        if self.docid  == other.docid:
            doc_diff[Document.DOCID]  = 0
        if self.name   == other.name:
            doc_diff[Document.NAME]   = 0
        if self.mode   == other.mode:
            doc_diff[Document.MODE]   = 0
        if self.mtime  == other.mtime:
            doc_diff[Document.MTIME]  = 0
        if self.sz     == other.sz:
            doc_diff[Document.SZ]     = 0
        if self.sha256 == other.sha256:
            doc_diff[Document.SHA256] = 0
        if self.data   == other.data:
            doc_diff[Document.DATA]   = 0

        # pdb.set_trace()
        meta_diff = [ d for d in MetadataList.diff(self.metadata, other.metadata) ]

        if len(meta_diff) == 0:
            doc_diff[Document.METADATA] = 0
        rval = [doc_diff,meta_diff]
        return rval


class SQLZip:
    """SQLZip provides uses SQLite3, compression libraries and 
    metadata to provide a file archive that can be searched for
    files matching one or more properties.
    """

    def __init__(self, database:str = ":memory:", 
                       create:bool  = False,
                       convert:bool = False,
                       **kwargs) -> None:


        if database != ":memory:":
            db_path = Path(database)
            if (db_path.exists() and db_path.is_file()) or create:
                self.connection = sql3.connect(database, **kwargs)
                if create:
                    convert = True
            else:
                raise FileNotFoundError(f"File not found: {database}")
        else:
            self.connection = sql3.connect(database, **kwargs)

        self.name = database
        self.connection.row_factory = sql3.Row
        self.sqlite3_major_version = sql3.sqlite_version_info[0]
        self.sqlite3_minor_version = sql3.sqlite_version_info[1]

        self.connection.create_function("sha256",1,sha256sum)
        self.connection.create_function("zlib_compress",1,zlib_compress)
        self.connection.create_function("zlib_decompress",1,zlib_decompress)
        self.connection.execute('PRAGMA foreign_keys=1')

        cursor = self.connection.cursor()
        self.compressor = 'zlib'

        # check to see if the archive has already been initialized

        """
        try:
#            res0 = self.connection.execute('SELECT COUNT(*) FROM sqlar')
            res0 = self.connection.execute('SELECT application_id from pragma_application_id()')
            app_id = res0.fetchone()[0]
            if app_id == 0x53515a50:
                pass
            elif app_id == 0 and convert:
                cursor.executescript(sql_20220105_002_up)
                cursor.executescript(sql_20220105_003_up)
                self.connection.commit()
            else:
                raise SQLZipError("Not a SQLZip archive and no convert flag set")
        except Exception as msg0:
            cursor.executescript(sql_20220105_001_up)
            cursor.executescript(sql_20220105_003_up)
            self.connection.commit()
        """
        self.update_migrations(self.connection,convert)

        res = cursor.execute('SELECT COUNT(*) FROM __archive_md')
        nitems = res.fetchone()
        if nitems[0] != 3:
            print("WARNING - incorrect number of items found in archive metadata",file=sys.stderr)
        else:
            res = self.connection.execute("SELECT value FROM __archive_md WHERE name = 'compressor'")
            self.compressor = res.fetchone()[0]
            if self.compressor and len(self.compressor) > 0 and self.compressor.casefold() != 'none':
                self.compress_lib = importlib.import_module(self.compressor)
            else:
                self.compress_lib = None
            res = self.connection.execute("SELECT value FROM __archive_md WHERE name = 'compression_level'")
            self.compression_level = res.fetchone()[0]

        cursor.close()

    def num_documents(self) -> int:

        cur = self.connection.cursor()
        res = cur.execute("SELECT count(*) as num_documents from sqlar")
        row = res.fetchone()
        return int(row[0])

    def close(self) -> None:
        self.connection.commit()
        self.connection.close()

    def create_document(self,
               document: Document,
               compressor:str = None,
              ) -> int:
        """Create a new entry in the archive, returning the docid of the document created, 
        or -1 if there was an error
        """

        if not document:
            print("No data was provided to create",file=sys.stderr)
            return -1

        if document.data and len(document.data) > 0:
            nd_hash = hashlib.sha256()
            try:
                nd_hash.update(document.data)
            except TypeError:
                document.data = bytearray(document.data)
                nd_hash.update(document.data)

            nd_sha256 = nd_hash.hexdigest()
            compresss_lib = None
            if compressor and len(compressor) > 0:
                compress_lib = importlib.import_module(compressor)
            else:
                compressor   = self.compressor
                compress_lib = self.compress_lib
        else:
            nd_sha256 = ""
            compressor = "none"
            compress_lib = None

        if compress_lib:
            nd_content = compress_lib.compress(document.data)
        else:
            nd_content = document.data

        cur = self.connection.cursor()
        cur.execute('BEGIN TRANSACTION')
        cur.execute('INSERT INTO sqlar (name,mode,mtime,sz,data,sha256) VALUES (?,?,?,?,?,?)',
                    (document.name,document.mode,document.mtime,document.sz,nd_content,nd_sha256))
        docid = cur.lastrowid

        md = []
        for m in document.metadata:
            md.append((m.name,m.value,docid))
        cur.executemany("INSERT INTO metadata (name,value,docid) VALUES (?,?,?)", md)
        self.connection.commit()

        return docid

    def delete_document(self,
               docid: int = None,
               name: str = None
              ) -> tp.Optional[Document]:
        """Delete a document from the archive given its docid or name, returning the original Document, or None
        """

        old_document = None
        if docid:
            old_document = self.read_document(docid=docid)
        elif name:
            old_document = self.read_document(name=name)
        if old_document:
            docid = old_document.docid
            cur = self.connection.cursor()
            cur.execute("BEGIN TRANSACTION")
            res = cur.execute("""DELETE FROM metadata WHERE docid = ?""",(docid,))
            res1 = cur.execute("""DELETE FROM sqlar WHERE rowid = ?""",(docid,))
            self.connection.commit()
            cur.close()

        return old_document

    def list_metadata_names(self) -> tp.List[str]:
        """List the names of metadata elements"""

        cur = self.connection.cursor()
        res = cur.execute("SELECT name, count(*) as count FROM metadata GROUP BY name")
        metadata_names = []
        for row in res:
            try:
                metadata_names.append(f"{row['name']:<24} {row['count']:>6d}" )
            except KeyError:
                pass
        
        return metadata_names

    def read_document(self,
                      docid: int = 0,
                      name: str = None
                      ) -> tp.Optional[Document]:
        """Read a document given its docid or name, returning a Document or None
        """

        res = None
        cur = self.connection.cursor()
        cur.execute("BEGIN TRANSACTION")
        if docid <= 0 and not name:
            print("SQLZip.extract - must supply either a docid or a filename")
            self.connection.commit()
            return None
        elif name and len(name) > 0:
            res = cur.execute(Document.read_query + "WHERE name = ?",(name,))
        elif docid > 0:
            res = cur.execute(Document.read_query + "WHERE docid = ?",(docid,))
        else:
            res = None
            
        if res:
            row = res.fetchone()
            if not row:
                self.connection.commit()
                return None

            docid        = row["docid"]
            doc_name     = row["name"]
            doc_mode     = row["mode"]
            doc_mtime    = row["mtime"]
            doc_sz       = row["sz"]
            doc_sha256   = row["sha256"]
            rawdata      = row["data"]
            compressor   = self.compressor
            compress_lib = self.compress_lib
            res = cur.execute("""SELECT name
                                      , value
                                      , docid
                                   FROM metadata
                                  WHERE docid = ?""",(docid,))
            rows = res.fetchall()
            if self.compress_lib is not None and rawdata is not None:
                doc_data = self.compress_lib.decompress(rawdata)
            else:
                doc_data = rawdata

            metadata = []
            for row in rows:
                md = Metadata(name=row[Metadata.NAME],value=row[Metadata.VALUE])
                metadata.append(md)
            
            document = Document(docid=docid,
                                name=doc_name,
                                mode=doc_mode,
                                mtime=doc_mtime,
                                data=doc_data,
                                metadata=metadata,
                                library=self)
            self.connection.commit()
            return document
        else:
            self.connection.commit()
            return None

    def update_document(self,
               new_doc: Document,
              ) -> tp.Optional[Document]:
        """Update a Document, returns the old Document or None
         if there was a problem.
         """

        if new_doc.docid == 0 and (not new_doc.name or len(new_doc.name) == 0):
            return None
        
        old_doc = None
        if new_doc.docid > 0:
            old_doc = self.extract_document(docid=new_doc.docid)
        else:
            old_doc = self.extract_document(name=new_doc.name)

        if old_doc and old_doc.docid > 0:
            if new_doc.data and len(new_doc.data) > 0:
                nd_hash = hashlib.sha256()
                try:
                    nd_hash.update(new_doc.data)
                except TypeError:
                    new_doc.data = new_doc.data.encode()
                    nd_hash.update(new_doc.data)

                nd_sha256 = nd_hash.hexdigest()
                compresss_lib = None
                if self.compressor and len(self.compressor) > 0:
                    compress_lib = importlib.import_module(self.compressor)
                else:
                    compressor   = self.compressor
                    compress_lib = self.compress_lib
            else:
                nd_sha256 = ""
                compressor = "none"
                compress_lib = None

            if compress_lib is not None:
                nd_content = compress_lib.compress(new_doc.data)
            else:
                nd_content = new_doc.data

            cur = self.connection.cursor()
            cur.execute('BEGIN TRANSACTION')
            cur.execute("""DELETE FROM metadata WHERE docid = ?""",(old_doc.docid,))
            cur.execute('UPDATE sqlar SET name=?,mode=?,mtime=?,sz=?,data=?,sha256=? WHERE docid = ?',
                        (new_doc.name,new_doc.mode,new_doc.mtime,new_doc.sz,nd_content,nd_sha256,old_doc.docid))

            md = []
            for m in new_doc.metadata:
                md.append((m.name,m.value,old_doc.docid))
            cur.executemany("INSERT INTO metadata (name,value,docid) VALUES (?,?,?)", md)
            self.connection.commit()
        
        return old_doc

    def update_document_metadata(self, dbconn, metadata_deltas):
        """update_document_metadata"""
        pass

    def update_migrations(self, dbconn, convert=None,create=None) -> None:
        """Update database migrations to the latest version."""

        parent_dir = Path(__file__).parent
        migration_dir = parent_dir / "migrations"
        migrations = [f for f in migration_dir.glob('*_up.sql')]
        migrations.sort()

        try:
            res0 = self.connection.execute('SELECT COUNT(*) FROM sqlar')
            res = dbconn.execute('SELECT application_id from pragma_application_id()')
            app_id = res.fetchone()[0]
            if app_id == 0x53515a50:      # SQZP
                res = dbconn.execute("SELECT name,type,value FROM __archive_md WHERE name == 'migration'")
                row = res.fetchone()
                if row is not None:
                    current_migration = row["value"]
                else:
                    current_migration = "0002_up.sql"
                for f in migrations:
                    if f.name > current_migration:
                        code = f.read_text(encoding="utf-8")
                        dbconn.executescript(code)
                        dbconn.commit()

            elif app_id == 0 and convert:
                del migrations[0]  # first migration defines the sqlar table for sqlite3's archive
                for f in migrations:
                    code = f.read_text(encoding="utf-8")
                    dbconn.executescript(code)
                dbconn.commit()
            elif create:
                for f in migrations:
                    code = f.read_text(encoding="utf-8")
                    dbconn.executescript(code)
                dbconn.commit()
            else:
                raise SQLZipError("Not a SQLZip archive and no convert flag set")
        except sql3.OperationalError:
            try:
                for f in migrations:
                    code = f.read_text(encoding="utf-8")
                    dbconn.executescript(code)
                dbconn.commit()
            except Exception as msg:
                raise SQLZIPError(f"Error received during migrations: {msg}")

class SQLZIPError(Exception):
    pass

header_formatter = string.Formatter()
data_formatter = string.Formatter()

class SQLZipCli:
    """
    SQLZipCli - a command line interface to the SQLZip library.

    python -m sqlzip -h

    """


    def metadata_from_args(self, arglist: tp.List[str]) -> tp.List[Metadata]:
        metadata = []
        if arglist:
            for m in arglist:
                (name,value) = m.split("=")
                md = Metadata(name=name,value=value)
                metadata.append(md)
        return metadata

    def add_cmd(self, args ):
        """add a document to the archive
        """
#        pdb.set_trace()
        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {args.db}",file=sys.stderr)
            exit()

        try:
            filename = args.file
            newitem = Path(filename)
            if filename.casefold() == 'none':
                metadata =  self.metadata_from_args(args.meta)
                docid = db.create_document(Document(name=None,data=None,metadata=metadata,mode=0,mtime=0))
                print("Created document docid={}",docid)
            elif newitem.exists():
                data = newitem.read_bytes()
                metadata = self.metadata_from_args(args.meta)
                stat = newitem.stat()
                docid = db.create_document(Document(name=filename,
                                            data=data,
                                            mode=stat.st_mode,
                                            mtime=stat.st_mtime,
                                            metadata=metadata
                                            ))
                print("Created document docid={}".format(docid))
            else:
                print("{} does not seem to exist".format(filename),file=sys.stderr)
        except Exception as msg:
            print(f"add_Cmd: {msg}")

    def delete_cmd(self, args ):
        """delete a document from the archive
        """
        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {args.db}",file=sys.stderr)
            exit()


        if args.docid:
            docid = int(args.docid)
            doc = db.delete_document(docid)
            if doc:
                print("Deleted document docid={}".format(doc))
            else:
                print("Document {} does not seem to exist".format(docid),file=sys.stderr)

    def initialize(self, args ):
        """Initialize a new database, checking to insure the file does not
        exist before creating it.
        """

        if self.args.verbose > 0:
            print("initialize infile: {}'".format(args.db))

        createfile = False
        newfile = Path(args.db)

        if newfile.exists():
            print("File, {} already exists, convert to SQLZip [y/N]?".format(newfile))
            resp = sys.stdin.readline().rstrip()
            if resp.casefold() != 'y':
                return None
            else:
                self.db = SQLZip(newfile,convert=True)
        else:
            self.db = SQLZip(newfile,create=True)


    def query_compile(self, args, db):
        """Compile the query into SQL
        """
        filters = []
        error = False
        if args.filters is not None:
            for f in args.filters:
                try:
                    filter = Filter(f)
                    filters.append(filter)
                except KeyError as msg:
                    print(msg, file=os.stderr)
                    error = True
            
            if not error:
                cur = db.connection.cursor()
                if args.md_only:
                    query_fields = ['docid','name','md_name','md_value']
                    query = Filter.compile(query_fields,filters,only_md_fields=args.only_md_fields)
                else:
                    query_fields = ['docid','name','mtime','size']
                    query = Filter.compile(query_fields,filters)
                cur.close()
                return (query_fields, query)
            else:
                return None
        else:
            print("No filters given")
            return None

    def query_results(self, query, db):
        cur = db.connection.cursor()
        results = query.execute(cur)
        for row in results:
            if self.args.md_only:
                data_cols = ()
                for colname in row.keys():
                    if colname == 'mtime' and row[colname]:
                        file_datetime = datetime.fromtimestamp(row[colname]).strftime("%Y-%m-%d %H:%M:%S")
                        data_cols     = data_cols+(file_datetime,)
                    elif colname == 'size' and row[colname]:
                        data_cols     = data_cols+(str(row[colname]),)
                    elif row[colname]:
                        data_cols     = data_cols+(row[colname],)
                    else:
                        data_cols     = data_cols+("",)
            else:
                data_cols = ()
                for colname in row.keys():
                    if colname == 'mtime' and row[colname]:
                        file_datetime = datetime.fromtimestamp(row[colname]).strftime("%Y-%m-%d %H:%M:%S")
                        data_cols     = data_cols+(file_datetime,)
                    elif colname == 'size' and row[colname]:
                        data_cols     = data_cols+(str(row[colname]),)
                    elif row[colname]:
                        data_cols     = data_cols+(row[colname],)
                    else:
                        data_cols     = data_cols+("",)
            yield data_cols


    def meta_cmd( self, args):
        """revise metadata associated with one or more files in the repository
        """

        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {args.db}",file=sys.stderr)
            exit()

        if args.list_metadata_names:
            names = db.list_metadata_names()
            for name in names:
                print(name)
            exit()

        if args.meta_add is not None and len(args.meta_add) > 0:
            meta_add_list = self.metadata_from_args(args.meta_add)

        if args.meta_del is not None and len(args.meta_del) > 0:
            meta_del_list = self.metadata_from_args(args.meta_del)

            
        if not args.dry_run:

            compile_results = self.query_compile( args, db)
            if compile_results is not None:
                (query_fields,query) = compile_results
                for row in self.query_results(query, db):
                    docid = row['docid']
                    

        else:
            qry = query.__repr__()
        

    def query_cmd(self, args ):
        """query the database for documents meeting the predicate filters
        """

#        pdb.set_trace()
        db = None
        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {args.db}",file=sys.stderr)
            exit()

        if args.list_metadata_names:
            names = db.list_metadata_names()
            for name in names:
                print(name)

        compile_results = self.query_compile( args, db)
        if compile_results is not None:
            (query_fields,query) = compile_results

            if not args.dry_run:
                header_str = "{:>10s} {:<24.24s} {:<20.20s} {:<32.32s}"
                data_format_str = "{:>10d} {:<24.24} {:<20.20} {:<32.32}"
                if args.md_only:
                    extra_cols = query.basic_fields
                    if extra_cols and len(extra_cols) > 0:
                        for c in extra_cols:
                            header_str = header_str + " {:<12.12}"
                            data_format_str = data_format_str + " {:<12.12}"
                        print(header_formatter.vformat(header_str,query_fields+extra_cols,None))
                    else:
                        print(header_formatter.vformat(header_str,query_fields,None))
                else:
                    print(header_formatter.vformat(header_str,query_fields,None))
    #                pdb.set_trace()
                    for row in self.query_results(query, db):
                        print(data_formatter.vformat(data_format_str,row,None))

            if args.print_sql:
                qry = query.__repr__()
                print(qry)
        else:
            print("No filters given")


    def extract_cmd(self, args ):
        """read the database for documents meeting the predicate filters
        """

        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {database}",file=sys.stderr)
            exit()

        filters = []
        error = False
        for f in args.filters:
            try:
                filter = Filter(f)
                filters.append(filter)
            except KeyError as msg:
                print(msg, file=os.stderr)
                error = True
        
        if not error:
            cur = db.connection.cursor()
            query = Filter.compile(['docid'],filters)
            if args.print_sql:
                qry = query.__repr__()
                print(qry)
            if not args.dry_run:
                results = query.execute(cur)
                for row in results:
                    doc = db.extract_document(docid=row[0])
                    # print("{}".format(doc))
                    

    def update_cmd(self, args ):
        """query the database for a document meeting the predicate filters
        """

        try:
            db = SQLZip(database=args.db)
        except FileNotFoundError as msg:
            print(f"File not found: {database}",file=sys.stderr)
            exit()

        filters = []

    def query_common_args(self, subcmd) -> None:

        subcmd.add_argument('--andall','-A',
                            action='append_const',
                            const=Filter.ANDALL,
                            dest='filters',
                            help='AND together all previous filters',
                           )
        subcmd.add_argument('--and','-a',
                            action='append_const',
                            const=Filter.AND,
                            dest='filters',
                            help='AND (intersect): -f "date >= 2021-01-01" -f "date < 2022-01-01" -a',
                           )
        subcmd.add_argument('--dry-run','-D',
                            action='store_true',
                            help='do everything, but do not execute query',
                           )
        subcmd.add_argument('--filter','-f',
                            action='append',
                            dest='filter',
                            help='filter for query, can repeat: -f "title like %%huckleberry%%"',
                           )
        subcmd.add_argument('--list-metadata','-l',
                            action='store_true',
                            dest='list_metadata_names',
                            help='list metadata fields in library and exit',
                           )
        subcmd.add_argument('--orall','-O',
                            action='append_const',
                            const=Filter.ORALL,
                            dest='filters',
                            help='OR all previous filters',
                           )
        subcmd.add_argument('--or','-o',
                            action='append_const',
                            const=Filter.OR,
                            dest='filters',
                            help='OR (union): -f "format == image/jpeg" -f "format == image/png" -o',
                           )
        subcmd.add_argument('--print','-p',
                            action='store_true',
                            dest='print_sql',
                            help='print the SQL query before executing it',
                           )

    def __init__(self):

        parser = ap.ArgumentParser(description="""Like a ZIP file, but with metadata and query""",
                                   prog="sqlzip.py")
        parser.add_argument( '--doctest',
            action='store_const',
            const=True,
            default=False,
            dest='doctest',
            help='run doctest on module'
        )
        parser.add_argument( '-v',
            action='count',
            default=0,
            dest='verbose',
            help='verbosity level, repeat for more verbosity',
        )
        parser.add_argument( '-V',
            action='store_const',
            const=True,
            default=False,
            dest='version',
            help='report package version and exit'
            )
        subcmds = parser.add_subparsers(description='sqlzip commands',
                                        dest='subcommand',
                                        help='additional help')

        subcmds_add = subcmds.add_parser('add', help='add a new file to the archive')
        subcmds_add.set_defaults(func=SQLZipCli.add_cmd)
        subcmds_add.add_argument('--meta','-m',
                                dest='meta',
                                help="""metadata for file, NAME=VALUE, repeatable, e.g.,
                                -m 'author=Brian W. Kernighan'
                                -m 'author=Dennis M. Ritchie'
                                -m 'title=The C Programming Language'
                                """,
                                action='append',
                                )
        subcmds_add.add_argument('db', help='database to use')
        subcmds_add.add_argument('file', help='file to add')

        subcmds_del = subcmds.add_parser('delete', help='delete an item from the archive')
        subcmds_del.set_defaults(func=SQLZipCli.delete_cmd)
        subcmds_del.add_argument('db', help='database to use')
        subcmds_del.add_argument('docid', help='document id of item to delete')

        subcmds_read = subcmds.add_parser('extract', help='extract file(s) from the archive')
        subcmds_read.set_defaults(func=SQLZipCli.extract_cmd)
        self.query_common_args(subcmds_read)
        subcmds_read.add_argument('db', help='database to use')

        subcmds_init = subcmds.add_parser('init', help='initialize a new SqlZIP archive')
        subcmds_init.set_defaults(func=SQLZipCli.initialize)
        subcmds_init.add_argument('db', 
                                  help='database file name to use for archive',
                                 )
#        subcmds_init.add_argument('--compressor', '-c',
#                                  dest='compressor',
#                                  default='zlib',
#                                  help='compression library to use: none, bz2, lzma, zlib (default)',
#                                 )
#        subcmds_init.add_argument('--compression_level', '-l',
#                                  dest='compression_level',
#                                  default=-1,
#                                  help='compression level to use, 0-9, or -1 for default',
#                                 )
        
        subcmds_meta = subcmds.add_parser('meta', help="""update metadata associated with documents
        """)
        subcmds_meta.set_defaults(func=SQLZipCli.meta_cmd)
        subcmds_meta.add_argument('--meta-add','-ma',
                                  dest='meta_add',
                                  help="""metadata for file, NAME=VALUE, repeatable, e.g.,
                                  -ma 'author=Brian W. Kernighan'
                                  -ma 'author=Dennis M. Ritchie'
                                  -ma 'title=The C Programming Language'
                                  """,
                                  action='append',
                                 )
        subcmds_meta.add_argument('--meta-del','-md',
                                  dest='meta_del',
                                  help="""delete metadata for file, NAME=VALUE, repeatable, e.g.,
                                  -md 'author=Brian W. Kernighan'
                                  -md 'author=Dennis M. Ritchie'
                                  -md 'title=The C Programming Language'
                                  Both NAME and VALUE must match for the metadata to be deleted for 
                                  files matching the query
                                  """,
                                  action='append',
                                 )
        subcmds_meta.add_argument('db', help='database to use')
        self.query_common_args(subcmds_meta)

        subcmds_qry = subcmds.add_parser('query', help="""query the archive --
        Individual filters may be combined using a reverse polish notation
        to create complex queries.  These will be compiled into a SQL query and executed.
        """)
        subcmds_qry.set_defaults(func=SQLZipCli.query_cmd)
        subcmds_qry.add_argument('db', help='database to use')
        subcmds_qry.add_argument('--metadata-fields','-M',
                                action='store',
                                dest='only_md_fields',
                                help='return only these metadata fields (comma separated list)',
                                )
        subcmds_qry.add_argument('--metadata-only','-m',
                                action='store_true',
                                dest='md_only',
                                help='return metadata only',
                                )
        self.query_common_args(subcmds_qry)

        subcmds_update = subcmds.add_parser('update', help='update a file in the archive')
        subcmds_update.set_defaults(func=SQLZipCli.update_cmd)
        subcmds_update.add_argument('db', help='database to use')

        args = parser.parse_args()
        self.args = args
        if args.version:
            version = pkg_resources.require("sqlzip")[0].version
            print(f"SQLZip version: {version}")
            exit(0)

        if args.doctest:
            doctest.testmod()
            exit(0)

        if 'func' in args:
            try:
                args.func(self, self.args)
            except Exception as msg:
                print(f"An error occurred: {msg}")

if __name__ == '__main__':
    sqlzip_app = SQLZipCli()
