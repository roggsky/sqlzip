/* 0003_up.sql - Add metadata to the archive
 
   This migration adds metadata for the archive, metadata for documents,
   a view for documents, and table to contain database migration data.
*/

BEGIN TRANSACTION;
CREATE TABLE __archive_md ( name  TEXT
                          , type  TEXT
                          , value TEXT
                          );
CREATE TABLE metadata   ( name  TEXT
                        , value TEXT
                        , docid INT  REFERENCES sqlar(docid)
                        );
CREATE UNIQUE INDEX metadata_idx  ON metadata(name,value,docid);
CREATE INDEX metadata_docid_idx ON metadata(docid,name);
CREATE TABLE metadata_names ( name TEXT PRIMARY KEY);
CREATE TRIGGER metadata_insert_trg
    AFTER INSERT ON metadata
        BEGIN
            INSERT OR IGNORE
              INTO metadata_names (name) VALUES (NEW.name)
            ;
        END;
CREATE TRIGGER metadata_update_trg
    AFTER UPDATE ON metadata
        BEGIN
            INSERT OR IGNORE
              INTO metadata_names (name) VALUES (NEW.name)
            ;
        END;
INSERT INTO __archive_md(name,type,value) 
     VALUES ('compressor','str','zlib')
          , ('compression_level','int','-1')
          , ('migration','str','0003_up.sql')
;
CREATE VIEW documents AS
    SELECT A.docid      as docid,
           A.name       as name,
           A.mtime      as mtime,
           A.mode       as mode,
           A.sz         as size,
           A.sha256     as sha256,
           A.compressor as compressor,
           B.name       as md_name,
           B.value      as md_value
      FROM sqlar A
 LEFT JOIN metadata B
        ON B.docid = A.docid
;
CREATE TABLE __migrations (
    name TEXT PRIMARY KEY
);
INSERT INTO __migrations (name)
    VALUES  ('0001_up.sql'),
            ('0002_up.sql'),
            ('0003_up.sql')
;
COMMIT;
