/* 0002_up.sql - migrate a Sqlite3 archive to a SQLZip archive

We have to create a temporary table since we use the original rowid
as the document identifier, docid, but the original sqlar table has
a primary key as the name field and, as I understand it, we cannot
drop the old primary index and create a new one, AND, when a vacuum
operation is performed, we are not guaranteed the rowid value remains
the same across vacuum operations.  Thus we create a new table with
the primary key being the rowid equivalent column, docid, and the 
data to the new table.  See https://sqlite.org/rowidtable.html for
all the details, specifically bullet items 1-6, especially the sixth
one.
*/

BEGIN TRANSACTION;
CREATE TABLE tmp_sqlar ( docid  INTEGER PRIMARY KEY  -- use docid instead of rowid
                       , name   TEXT NOT NULL        -- name 
                       , mode   INT                  -- access permissions
                       , mtime  INT                  -- last modification time
                       , sz     INT                  -- original file size
                       , data   BLOB                 -- compressed content
                       , sha256 BLOB                 -- SHA256 hash to detect data corruption
                       , compressor TEXT             -- allow for different compression
                       );

INSERT INTO tmp_sqlar
    SELECT rowid  as docid
         , name   as name
         , mode   as mode
         , mtime  as mtime
         , sz     as sz
         , data   as data
         , NULL   as sha256
         , 'zlib' as compressor
      FROM sqlar
    ORDER BY rowid ASC;
DROP TABLE sqlar;
ALTER TABLE tmp_sqlar RENAME TO sqlar;
CREATE INDEX sqlar_name_idx ON sqlar(name);
PRAGMA application_id = 0x53515a50;  -- SQZP
COMMIT;
VACUUM;
