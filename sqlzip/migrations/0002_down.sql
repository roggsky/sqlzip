/*  0002_down.sql -- revert 00002_up.sql

    This is essentialliy a no-op since 0002_up just copies the data to
    a new table that is mimics the original sqlar table.  It adds two
    additional columns and exposes the rowid column as the document id
    column, docid, but this becomes an alias for rowid due to it being
    declared as an INTEGER PRIMARY KEY.

    The solution is left as an exercise for the reader, if this is
    problematic.
*/


