/* migration 0001_up.sql
   This migration duplicates the table created using the 
   sqlite3 archive table as the SQLZip archive uses it
   as the base
*/

BEGIN TRANSACTION;
CREATE TABLE sqlar(
  name TEXT PRIMARY KEY,  -- name of the file
  mode INT,               -- access permissions
  mtime INT,              -- last modification time
  sz INT,                 -- original file size
  data BLOB               -- compressed content
);
COMMIT;
