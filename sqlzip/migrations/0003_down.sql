/* 0003_down.sql - revert 0003_up.sql
 */

BEGIN TRANSACTION;
DROP TABLE IF EXISTS __migrations;
DROP VIEW  IF EXISTS documents;
DROP TRIGGER IF EXISTS metadata_update_trg;
DROP TRIGGER IF EXISTS metadata_insert_trg;
DROP TABLE   IF EXISTS metadata_names;
DROP INDEX   IF EXISTS metadata_docid_idx;
DROP TABLE   IF EXISTS metadata;
DROP TABLE   IF EXISTS __archive_md;
COMMIT;

