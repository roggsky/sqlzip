import unittest
import sqlzip.sqlzip as sz

testdoc_name_01 = "Gettysburg Address"
testdoc_content_01 = b"""Four score and seven years ago,..."""
testdoc_metadata_01 = [ sz.Metadata(name='author',  value='Lincoln, Abraham'),
                        sz.Metadata(name='date',    value='1863-11-19'),
                        sz.Metadata(name='title',   value='Gettysburg Address'),
                        sz.Metadata(name='location',value='Gettysburg, PA, USA'),
                        sz.Metadata(name='subject', value='abraham lincoln'),
                        sz.Metadata(name='subject', value='presidential speech'),
                        sz.Metadata(name='subject', value='us civil war'),
                        sz.Metadata(name='subject', value='us president'),
                        sz.Metadata(name='format',  value='text/plain'),
                      ]
testdoc_01   = sz.Document(name=testdoc_name_01,data=testdoc_content_01,metadata=testdoc_metadata_01)

testdoc_delmeta_01 = [ sz.Metadata(name='date',    value='1863-11-19'), ]
testdoc_addmeta_01 = [ sz.Metadata(name='date',    value='1963-11-19'), ]

testdoc_name_02 = "The Adventures of Huckleberry Finn"
testdoc_content_02 = b"""You don\'t know about me ..."""
testdoc_metadata_02 = [ sz.Metadata(name='author', value='Clemens, Samuel Langhorn'),
                        sz.Metadata(name='author', value='Twain, Mark'),
                        sz.Metadata(name='date',   value='1884'),
                        sz.Metadata(name='format', value='text/plain'),
                        sz.Metadata(name='title',  value='The Adventures of Huckleberry Finn'),
                      ]
testdoc_02   = sz.Document(name=testdoc_name_02,data=testdoc_content_02,metadata=testdoc_metadata_02)

testdoc_name_03 = "Gettysburg Address"
testdoc_content_03 = b"""Four score and seven years ago,..."""
testdoc_metadata_03 = [ sz.Metadata(name='author',  value='Lincoln, Abraham'),
                        sz.Metadata(name='date',    value='1863-11-19'),
                        sz.Metadata(name='title',   value='Gettysburg Address'),
                        sz.Metadata(name='location',value='Gettysburg, PA, USA'),
                        sz.Metadata(name='subject', value='abraham lincoln'),
                        sz.Metadata(name='subject', value='presidential speech'),
                        sz.Metadata(name='subject', value='us civil war'),
                        sz.Metadata(name='subject', value='us president'),
                        sz.Metadata(name='format',  value='text/plain'),
                      ]
testdoc_03   = sz.Document(name=testdoc_name_03,data=testdoc_content_03,metadata=testdoc_metadata_03)

testdoc_name_04 = r"Apostrophe's Many''s test'''s"
testdoc_content_04 = b"""Testing apostrophe handling"""
testdoc_metadata_04 = [ sz.Metadata(name='author',  value='SQLzip maintainers'),
                        sz.Metadata(name='title',   value=r"Apostrophe's Many''s test'''s"),
                      ]
testdoc_04   = sz.Document(name=testdoc_name_04,data=testdoc_content_04,metadata=testdoc_metadata_04)

diffdoc_name_00 = "The Adventures of Huckleberry Finn"
diffdoc_content_00 = b"""You don\'t know about me ..."""
diffdoc_metadata_00 = [ sz.Metadata(name='author', value='Clemens, Samuel Langhorn'),
                        sz.Metadata(name='author', value='Twain, Mark'),
                        sz.Metadata(name='date',   value='1884'),
                        sz.Metadata(name='format', value='text/plain'),
                        sz.Metadata(name='title',  value='The Adventures of Huckleberry Finn'),
                      ]
diffdoc_00   = sz.Document(docid=1,
                           name=diffdoc_name_00,
                           mode=3460,
                           data=diffdoc_content_00,
                           metadata=diffdoc_metadata_00)

diffdoc_name_01 = "The Adventures of Huck Finn"
diffdoc_content_01 = b"""You don\'t know me ..."""
diffdoc_metadata_01 = [ sz.Metadata(name='author', value='Clemens, Samuel Langhorn'),
                        sz.Metadata(name='date',   value='1884'),
                        sz.Metadata(name='format', value='text/plain'),
                        sz.Metadata(name='subject', value='Huckleberry Finn'),
                        sz.Metadata(name='title',  value='The Adventures of Huck Finn'),
                      ]
diffdoc_01   = sz.Document(docid=1,
                           name=diffdoc_name_01,
                           mode=3464,
                           data=diffdoc_content_01,
                           metadata=diffdoc_metadata_01)

diffdoc_00_01 = [[0,1,1,0,1,1,1,1],
                  [(sz.MetadataList.MDL_MISSING_SELF,  'author', 'Twain, Mark'), 
                   (sz.MetadataList.MDL_MISSING_OTHER, 'subject', 'Huckleberry Finn'), 
                   (sz.MetadataList.MDL_DIFFER,        'title', 'The Adventures of Huckleberry Finn', 'The Adventures of Huck Finn')
                  ]]

filters = {
    "lincoln":    sz.Filter("author==Lincoln, Abraham"),
    "gettysburg": sz.Filter("title like %Gettysburg%"),
    "gettysname": sz.Filter("name like %Gettysburg%"),
    "ga_date":    sz.Filter("date==1863-11-19"),
    "ga_location":sz.Filter("location=Gettysburg, PA, USA"),
    "format":     sz.Filter("format=text/plain"),
    "twain":      sz.Filter("author==Twain, Mark"),
    "hf_date":    sz.Filter("date==1884"),
    "huckfinn":   sz.Filter("title like %Huckleberry%"),
    "apostrophe": sz.Filter(r"title==Apostrophe's Many''s test'''s"),
    "and":        sz.Filter(sz.Filter.AND),
    "andall":     sz.Filter(sz.Filter.ANDALL),
    "or":         sz.Filter(sz.Filter.OR),
    "orall":      sz.Filter(sz.Filter.ORALL),
}


class SQLZipTestCase(unittest.TestCase):

    def setUp(self):
        self.db = sz.SQLZip()

    def test_metadata_creation(self):
        md = sz.Metadata(name='author',value='Mark Twain')
        self.assertEqual(md.name,'author')
        self.assertEqual(md.value,'Mark Twain')

    def test_metadata_eq(self):
        md1 = sz.Metadata(name='author',value='Twain, Mark')
        md2 = sz.Metadata(name='author',value='Tolstoy, Leo')
        md3 = sz.Metadata(name='author',value='Twain, Mark')
        self.assertTrue(md1 == md3)
        self.assertFalse(md1 == md2)
        self.assertTrue(md1 != md2)

    def test_metadata_lt(self):
        md1 = sz.Metadata(name='author',value='Twain, Mark')
        md2 = sz.Metadata(name='author',value='Tolstoy, Leo')
        md3 = sz.Metadata(name='author',value='Twain, Mark')
        self.assertTrue(md2 < md3)
        self.assertFalse(md1 < md3)

    def test_metadata_tolerates_none_value(self):
        md1 = sz.Metadata(name='author',value=None)
        self.assertIsNotNone(md1)

    def test_metadata_none_value_is_maximal(self):
        md1 = sz.Metadata(name='author',value=None)
        md2 = sz.Metadata(name='author',value='Mark Twain')
        self.assertTrue(md1 > md2)


    def test_document_creation(self):
        doc = sz.Document(name=testdoc_name_01,
                          data=testdoc_content_01,
                          metadata=testdoc_metadata_01,
                         )
        self.assertTrue(doc.name == testdoc_name_01)

    def test_document_eq(self):
        doc1 = sz.Document(name=testdoc_name_01,
                          data=testdoc_content_01,
                          metadata=testdoc_metadata_01,
                         )
        doc2 = sz.Document(name=testdoc_name_02,
                          data=testdoc_content_02,
                          metadata=testdoc_metadata_02,
                         )
        doc3 = sz.Document(name=testdoc_name_03,
                          data=testdoc_content_03,
                          metadata=testdoc_metadata_03
                         )
        self.assertTrue(doc1 == doc3)
        self.assertTrue(doc1 != doc2)

    def test_db_created(self):
        self.assertEqual(self.db.num_documents(), 0, 'database not created')

    def test_document_created(self):
        docid = self.db.create_document(testdoc_01)
        self.assertEqual(docid, 1, 'incorrect document id returned')

    def test_document_diff(self):
        diff = [ d for d in diffdoc_00.diff(diffdoc_01) ]
        self.assertTrue(diff == diffdoc_00_01)

    def test_document_read(self):
        docid  = self.db.create_document(testdoc_01)
        newdoc = self.db.read_document(docid=docid)
        self.assertTrue(newdoc == testdoc_01)

        newdoc1 = self.db.read_document(name=testdoc_name_01)
        self.assertTrue(newdoc1 == testdoc_01)

    def test_document_compression(self):

        docid  = self.db.create_document(testdoc_01,compressor='zlib')
        newdoc = self.db.read_document(docid=docid)
        newdoc.data = newdoc.content()
        self.assertTrue(newdoc == testdoc_01)

    def test_document_metadata_only(self):

        docid  = self.db.create_document(testdoc_01)
        newdoc = self.db.read_document(docid=docid)
        self.assertTrue(newdoc == testdoc_01)

    def test_document_delete_via_docid(self):

        # create the document
        docid  = self.db.create_document(testdoc_01)

        # insure the document was created
        newdoc = self.db.read_document(docid=docid)
        self.assertTrue(newdoc == testdoc_01)

        # delete the document
        old_document = self.db.delete_document(docid=docid)
        self.assertTrue(old_document == testdoc_01)

        # insure it was deleted
        no_document = self.db.delete_document(docid=docid)
        self.assertTrue(no_document == None)

    def test_document_delete_via_name(self):

       # create the document
        docid  = self.db.create_document(testdoc_01)

        # insure the document was created
        newdoc = self.db.read_document(docid=docid)
        self.assertTrue(newdoc == testdoc_01)

        # delete the document
        old_document = self.db.delete_document(name=testdoc_name_01)
        self.assertTrue(old_document == testdoc_01)

        # insure it was deleted
        no_document = self.db.delete_document(docid=docid)
        self.assertTrue(no_document == None)

    
    def test_document_update(self):

       # create the document
        docid  = self.db.create_document(testdoc_01)

        # insure the document was created
        newdoc = self.db.read_document(docid=docid)
        self.assertTrue(newdoc == testdoc_01)

        # update the document
        docid = newdoc.docid
        updated_doc = testdoc_02
        updated_doc.docid = docid
        old_document = self.db.update_document(updated_doc)
        self.assertTrue(old_document == testdoc_01)

        # insure it was deleted
        updated_document = self.db.read_document(docid=docid)
        self.assertTrue(updated_document == testdoc_02)

    def test_filter_init(self):
        filter1 = sz.Filter("author==Arthur C. Clarke")
        self.assertTrue(filter1.name == 'author')
        self.assertTrue(filter1.predicate == '==')
        self.assertTrue(filter1.value == 'Arthur C. Clarke')

        filter2 = sz.Filter("date!=2020-01-01")
        self.assertTrue(filter2.name == 'date')
        self.assertTrue(filter2.predicate == '!=')
        self.assertTrue(filter2.value == '2020-01-01')

        filter3 = sz.Filter("date>=2020-01-01")
        self.assertTrue(filter3.name == 'date')
        self.assertTrue(filter3.predicate == '>=')
        self.assertTrue(filter3.value == '2020-01-01')

        filter4 = sz.Filter("date>2020-01-01")
        self.assertTrue(filter4.name == 'date')
        self.assertTrue(filter4.predicate == '>')
        self.assertTrue(filter4.value == '2020-01-01')

        filter5 = sz.Filter("date<=2020-01-01")
        self.assertTrue(filter5.name == 'date')
        self.assertTrue(filter5.predicate == '<=')
        self.assertTrue(filter5.value == '2020-01-01')

        filter6 = sz.Filter("date<2020-01-01")
        self.assertTrue(filter6.name == 'date')
        self.assertTrue(filter6.predicate == '<')
        self.assertTrue(filter6.value == '2020-01-01')

        filter7 = sz.Filter("date <2020-01-01")
        self.assertTrue(filter7.name == 'date')
        self.assertTrue(filter7.predicate == '<')
        self.assertTrue(filter7.value == '2020-01-01')

        filter8 = sz.Filter("date ==2020-01-01")
        self.assertTrue(filter8.name == 'date')
        self.assertTrue(filter8.predicate == '==')
        self.assertTrue(filter8.value == '2020-01-01')

        filter9 = sz.Filter("date== 2020-01-01")
        self.assertTrue(filter9.name == 'date')
        self.assertTrue(filter9.predicate == '==')
        self.assertTrue(filter9.value == '2020-01-01')

        filterA = sz.Filter("date == 2020-01-01")
        self.assertTrue(filterA.name == 'date')
        self.assertTrue(filterA.predicate == '==')
        self.assertTrue(filterA.value == '2020-01-01')

        filterB = sz.Filter("date like %-01-%")
        self.assertTrue(filterB.name == 'date')
        self.assertTrue(filterB.predicate == 'like')
        self.assertTrue(filterB.value == '%-01-%')

        filterC = sz.Filter("@AND@")
        self.assertTrue(filterC.name == '@AND@')
        self.assertIsNone(filterC.predicate)
        self.assertIsNone(filterC.value)

        filterD = sz.Filter("@ANDALL@")
        self.assertTrue(filterD.name == '@ANDALL@')
        self.assertIsNone(filterD.predicate)
        self.assertIsNone(filterD.value)

        filterE = sz.Filter("@OR@")
        self.assertTrue(filterE.name == '@OR@')
        self.assertIsNone(filterE.predicate)
        self.assertIsNone(filterE.value)

        filterF = sz.Filter("@ORALL@")
        self.assertTrue(filterF.name == '@ORALL@')
        self.assertIsNone(filterF.predicate)
        self.assertIsNone(filterF.value)

    def test_filter_compile(self):

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["lincoln"],
                                             filters["and"],
                                             filters["hf_date"],
                                             filters["or"],
                                            ])  #,filters["ga_date"],filters["and"]])
#        print()
#        print(query.select)
#        print(query.parms)
#        print()
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ? UNION SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(query.parms == ['format','text/plain','author','Lincoln, Abraham','date','1884'])

        query = sz.Filter.compile(['docid','name'], [filters["format"],
                                                     filters["lincoln"],
                                                     filters["hf_date"],
                                                     filters["andall"],
                                  ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid,name FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(query.parms == ['format','text/plain','author','Lincoln, Abraham','date','1884'])

        query = sz.Filter.compile(['docid','name'], [filters["format"],
                                                     filters["lincoln"],
                                                     filters["hf_date"],
                                                     filters["orall"],
                                                    ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid,name FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ? UNION SELECT docid FROM documents WHERE md_name=? AND md_value == ? UNION SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(query.parms == ['format','text/plain','author','Lincoln, Abraham','date','1884'])

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["and"],
                                            ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ?))")
        self.assertTrue(query.parms == ['format','text/plain'])

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["andall"],
                                            ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ?))")
        self.assertTrue(query.parms == ['format','text/plain'])

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["or"],
                                            ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ?))")
        self.assertTrue(query.parms == ['format','text/plain'])

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["orall"],
                                            ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ?))")
        self.assertTrue(query.parms == ['format','text/plain'])

        query = sz.Filter.compile(['docid','name'], [filters["format"],
                                                     filters["lincoln"],
                                                     filters["hf_date"],
                                                     filters["andall"],
                                                     filters["and"],
                                  ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid,name FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(query.parms == ['format','text/plain','author','Lincoln, Abraham','date','1884'])



    def test_filter_gettysname(self):

        query = sz.Filter.compile(['docid'],[filters["gettysname"],
                                            ])  #,filters["ga_date"],filters["and"]])
        self.assertTrue(query.select == "SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE name like ?))")
        self.assertTrue(query.parms == ['%Gettysburg%'])



    def test_filter_execute(self):

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid','name'], [filters["format"],
                                                     filters["lincoln"],
                                                     filters["and"],
                                                     filters["hf_date"],
                                                     filters["or"],
                                                    ])  #,filters["ga_date"],filters["and"]])

        cur = self.db.connection.cursor()
        cur = query.execute(cur)
        res = []
        for row in cur.fetchall():
            res.append((row[0],row[1],))
        # sz.Filter.execute()
#        print()
#        print(res)
#        print()

        self.assertTrue(query.select == "SELECT DISTINCT docid,name FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value = ? INTERSECT SELECT docid FROM documents WHERE md_name=? AND md_value == ? UNION SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(query.parms == ['format','text/plain','author','Lincoln, Abraham','date','1884'])
        self.assertTrue( res == [(1,"Gettysburg Address",),(2,"The Adventures of Huckleberry Finn",)])

    def test_bad_chars(self):

        docid_4 = self.db.create_document(testdoc_04)
        cur = self.db.connection.cursor()
        query = sz.Filter.compile(['docid'],[filters["apostrophe"],])
#        print()
#        print(query)
#        print()
        cur = query.execute(cur)
        row = cur.fetchone()
        docid = row[0]
        # print("docid: {}".format(row[0]))

        self.assertTrue(query.select == r"SELECT DISTINCT docid FROM documents WHERE docid IN (SELECT DISTINCT docid FROM (SELECT docid FROM documents WHERE md_name=? AND md_value == ?))")
        self.assertTrue(docid == 1)

    def test_meta_change(self):

        docid_1  = self.db.create_document(testdoc_01)
        docid_2  = self.db.create_document(testdoc_02)
        query = sz.Filter.compile(['docid'],[filters["format"],
                                             filters["lincoln"],
                                             filters["and"],
                                            ])  #,filters["ga_date"],filters["and"]])

        cur = self.db.connection.cursor()
        cur = query.execute(cur)
        res = []
        for row in cur.fetchall():
            res.append(row[0])

        self.assertTrue(len(res) == 1)

        for docid in res:
            doc = self.db.read_document(docid=docid)
            doc.delete_metadata(testdoc_delmeta_01)
            doc.add_metadata(testdoc_addmeta_01)

        query = sz.Filter.compile(['docid'], [sz.Filter("date==1863-11-19"),])
        cur = self.db.connection.cursor()
        cur = query.execute(cur)
        res = []
        for row in cur.fetchall():
            res.append(row[0])

        self.assertTrue(len(res) == 0)

        query = sz.Filter.compile(['docid'], [sz.Filter("date==1963-11-19"),])

        cur = self.db.connection.cursor()
        cur = query.execute(cur)
        res = []
        for row in cur.fetchall():
            res.append(row[0])

        self.assertTrue(len(res) == 1)
