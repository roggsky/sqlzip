# SQLZIP

SQLZIP, like a ZIP file, but with additional metadata for each item so one can search for items in ways not posssible with a standard archive file.

SQLZIP combines SQLite3 wih zlib compression to offer a flexible way to store files and their associated metadata.

This project provides:

  - the SQLZIP library module providing the basic functionality
  - a command line aplication providing an in-depth example of how to use the SQLZIP library

## Getting started

TBW
